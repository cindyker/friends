package com.minecats.cindyk.friends;

import org.bukkit.ChatColor;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;

import static org.bukkit.Bukkit.getServer;

/**
 * Created by cindy on 4/7/14.
 */
public class FriendFunctions {

    Friends plugin;

    FriendFunctions(Friends plugin)
    {
        this.plugin = plugin;
    }

    void AddFriend(Player player, String FriendNames)
    {

        //MYSQL.....
        if(plugin.getConfig().getBoolean("Friends.MySQL.Enabled"))
        {

        //ToDo: MySql Implementation
        }
        else  //YAML....
        {

            MyFriends me = plugin.friendsInfo.GetPlayer(player.getName().toLowerCase().trim());
            if(me==null)
            {
                me = new MyFriends();
                plugin.friendsInfo.AddPlayer(player.getName().toLowerCase().trim(),me);
                UpdatePlayerSection(player.getName().toLowerCase().trim(), me);
            }

            if(me.deny)
            {
                plugin.fMessage(player, plugin.msgs.getMessage("CMD_DENY_NOTICE"));
                return ;
            }

            if(me.list.contains(FriendNames.toLowerCase().trim()))
            {
                plugin.fMessage(player,plugin.msgs.getMessage("CMD_ADD_ALREADY_ADDED"));
                return ;
            }



            String friend = FriendNames;
            Player pFriend = getServer().getPlayer(friend.toLowerCase().trim());
            if (pFriend==null)
            {
                plugin.fMessage(player,plugin.msgs.getMessage("CMD_ADD_NOT_ONLINE"));
                return;
            }

            //Add Friend to Pending List, Send Friend Request for Acceptance.
            MyFriends mf = plugin.friendsInfo.GetPlayer(pFriend.getName().toLowerCase().trim());
            if(mf==null)
            {
                mf = new MyFriends();

            }

            //////////////////////////////////////////////////////////////////////////////////////////
            if(mf.GetDeny())
            {
                plugin.fMessage(player,plugin.msgs.getMessage("CMD_ADD_FRIEND_DENY")+pFriend.getName());
                return;
            }

            if(mf.pending.contains(player.getName().toLowerCase().trim()) )
            {
                plugin.fMessage(player,plugin.msgs.getMessage("CMD_ADD_IS_PENDING"));
                return;
            }
            mf.AddPending(player.getName().toLowerCase().trim());
            plugin.friendsInfo.AddPlayer(pFriend.getName().toLowerCase().trim(),mf);
            UpdatePlayerSection(pFriend.getName().toLowerCase().trim(), mf);

            plugin.fMessage(pFriend,plugin.msgs.getMessage("CMD_ADD_REQUEST",player.getName()));
            // fMessage(pFriend,ChatColor.GOLD+player.getName()+ChatColor.WHITE+" has requested to be on your friends list.");
            plugin.fMessage(pFriend,plugin.msgs.getMessage("CMD_ADD_ACCEPT",player.getName()));
            // fMessage(pFriend,ChatColor.GOLD+"/friends accept " +  player.getName() + ChatColor.WHITE+" to accept the request");
            plugin.fMessage(pFriend,plugin.msgs.getMessage("CMD_ADD_DECLINE",player.getName()));
            //fMessage(pFriend,ChatColor.GOLD+"/friends decline " +  player.getName() + ChatColor.WHITE+" to decline the request");

            plugin.fMessage(player,plugin.msgs.getMessage("CMD_ADD_SENT_REQUEST",friend));
            //fMessage(player,"Sending Friends request to "+friend);


        }



    }

    public void AcceptFriend(Player player, String FriendName)
    {

        //MYSQL.....
        if(plugin.getConfig().getBoolean("Friends.MySQL.Enabled"))
        {
                //ToDo: MySQL IMPLEMENTATION for ACCEPT!

        }
        else
        {
            MyFriends mf1 = plugin.friendsInfo.GetPlayer(player.getName().toLowerCase().trim());
            MyFriends mf2 = plugin.friendsInfo.GetPlayer(FriendName.toLowerCase().trim());

            if(mf2==null)
            {
                plugin.fMessage(player, plugin.msgs.getMessage("CMD_ACCEPT_CANT_FIND"));
                //fMessage(player,"Friends: Can not find information on that player. Please double check the name.");
                return;
            }

            for(String pf : mf1.pending)
            {
                FriendName = FriendName.toLowerCase().trim();
                if(pf.compareToIgnoreCase(FriendName)==0)
                {
                    mf1.RemovePending(FriendName);
                    mf1.AddToList(FriendName);
                    mf2.AddToList(player.getName().toLowerCase().trim());

                    plugin.fMessage(player, plugin.msgs.getMessage("CMD_ACCEPT_ACCEPTED", pf));
                    // fMessage(player,"Friends list addition accepted for " + pf);
                    if(plugin.getServer().getPlayer(pf)!=null)
                    {
                        plugin.fMessage(plugin.getServer().getPlayer(pf), plugin.msgs.getMessage("CMD_ACCEPT_ACCEPTED_FRIEND", player.getName()));
                        //fMessage(plugin.getServer().getPlayer(pf),player.getName()+ " has accepted your friends request.");
                    }
                    else
                    {
                        mf2.AddNotice(plugin.msgs.getMessage("CMD_ACCEPT_ACCEPTED_FRIEND",player.getName()));
                        //mf2.AddNotice(player.getName()+ " has accepted your friends request.");
                    }

                    plugin.friendsInfo.AddPlayer(player.getName().toLowerCase().trim(),mf1);
                    UpdatePlayerSection(player.getName().toLowerCase().trim(), mf1);
                    plugin.friendsInfo.AddPlayer(FriendName,mf2);
                    UpdatePlayerSection(FriendName,mf2);
                    return;
                }

            }

        }

        plugin.fMessage(player, plugin.msgs.getMessage("CMD_ACCEPT_CANT_FIND"));
        // fMessage(player,"Friends: Can not find information on that player. Please double check the name.");
    }


 
    
    public void DeclineFriend(Player player, String FriendName)
    {

        //MYSQL.....
        if(plugin.getConfig().getBoolean("Friends.MySQL.Enabled"))
        {
            //ToDo: MySQL IMPLEMENTATION for ACCEPT!

        }
        else
        {


            FriendName = FriendName.toLowerCase().trim();
            MyFriends mf1 = plugin.friendsInfo.GetPlayer(player.getName().toLowerCase().trim());
            MyFriends mf2 = plugin.friendsInfo.GetPlayer(FriendName);

            if(mf2==null)
            {
                plugin.fMessage(player,plugin.msgs.getMessage("CMD_DECLINE_CANT_FIND"));
                //fMessage(player,"Friends: Can not find information on that player. Please double check the name.");
                return;
            }

            for(String pf : mf1.pending)
            {
                if(pf.compareToIgnoreCase(FriendName.trim())==0)
                {
                    mf1.RemovePending(FriendName.toLowerCase().trim());
                    plugin.fMessage(player,plugin.msgs.getMessage("CMD_DECLINE_REMOVED",pf));
                    //fMessage(player,"Friends list addition removed for " + pf);
                    if(plugin.getServer().getPlayer(pf.toLowerCase().trim())!=null)
                    {
                        plugin.fMessage(plugin.getServer().getPlayer(pf.toLowerCase().trim()),plugin.msgs.getMessage("CMD_DECLINE_REMOVED_FRIEND",player.getName()));
                        //fMessage(plugin.getServer().getPlayer(pf.toLowerCase().trim()),player.getName()+ " has declined your friends request.");
                    }
                    else
                    {
                        mf2.AddNotice(plugin.msgs.getMessage("CMD_DECLINE_REMOVED_FRIEND",player.getName()));
                        //mf2.AddNotice(player.getName()+ " has declined your friends request.");
                    }

                    plugin.friendsInfo.AddPlayer(player.getName().toLowerCase(),mf1);
                    UpdatePlayerSection(player.getName().toLowerCase(), mf1);
                    plugin.friendsInfo.AddPlayer(FriendName,mf2);
                    UpdatePlayerSection(FriendName,mf2);
                    return ;
                }

            }
        }

        plugin.fMessage(player,plugin.msgs.getMessage("CMD_DECLINE_CANT_FIND"));
        //fMessage(player,"Friends: Can not find information on that player. Please double check the name.");
    }


    public void RemoveFriend(Player player, String FriendName)
    {

        //MYSQL.....
        if(plugin.getConfig().getBoolean("Friends.MySQL.Enabled"))
        {
            //ToDo: MySQL IMPLEMENTATION for Remove!

        }
        else
        {

            MyFriends mf1 = plugin.friendsInfo.GetPlayer(player.getName().toLowerCase().trim());
            MyFriends mf2 = plugin.friendsInfo.GetPlayer(FriendName.toLowerCase().trim());

            FriendName = FriendName.toLowerCase().trim();

            plugin.getLogger().info(player.getName() + " is removing " + FriendName);

            if(mf1!=null)
            {
                if(mf1.deny)
                {
                    plugin.fMessage(player, plugin.msgs.getMessage("CMD_DENY_NOTICE"));
                    //fMessage(player,"You are currently denying all friends requests. /friends allow to change it");
                    return;
                }
            }

            if(mf1==null)
            {
                plugin.fMessage(player, plugin.msgs.getMessage("CMD_REMOVE_CANT_FIND_LIST"));
                //fMessage(player,"You do not have anyone on your list to remove. /friends help");
                return;
            }

            if(mf2==null)
            {
                plugin.fMessage(player, plugin.msgs.getMessage("CMD_REMOVE_CANT_FIND"));
                // fMessage(player,"Friends: Can not find information on that player. Please double check the name.");
                return ;
            }


            for(String pf : mf1.list)
            {
                if(pf.compareToIgnoreCase(FriendName)==0)
                {
                    mf1.RemoveListing(FriendName);
                    mf2.RemoveListing(player.getName().toLowerCase().trim());
                    plugin.fMessage(player, plugin.msgs.getMessage("CMD_REMOVE_REMOVED", pf));
                    //fMessage(player,"Removed "+pf+" from Friends list. Sending notice to player.");
                    if(plugin.getServer().getPlayer(pf.trim())!=null)
                    {
                        plugin.fMessage(plugin.getServer().getPlayer(pf), plugin.msgs.getMessage("CMD_REMOVE_NOTICE", player.getName()));
                        //fMessage(plugin.getServer().getPlayer(pf),player.getName()+ " has removed you from their friends list.");
                    }
                    else
                    {
                        mf2.AddNotice(plugin.msgs.getMessage("CMD_REMOVE_NOTICE",player.getName()));
                    }

                    plugin.friendsInfo.AddPlayer(player.getName().toLowerCase().trim(),mf1);
                    UpdatePlayerSection(player.getName().toLowerCase().trim(), mf1);
                    plugin.friendsInfo.AddPlayer(FriendName,mf2);
                    UpdatePlayerSection(FriendName,mf2);
                    return ;
                }

            }

            plugin.fMessage(player, plugin.msgs.getMessage("CMD_REMOVE_CANT_FIND"));
            //fMessage(player,"Friends: Can not find information on that player. Please double check the name.");



        }


    }

    
    public void Notify(Player player,String cmd)
    {
        MyFriends mf = plugin.friendsInfo.GetPlayer(player.getName().toLowerCase().trim());

        if(mf!=null)
        {
            if(mf.deny)
            {
                plugin.fMessage(player, plugin.msgs.getMessage("CMD_NOTIFY_DENY_ON"));
                //fMessage(player,"You are currently denying all friends requests. /friends allow to change it");
                return;
            }
        }

        if(cmd.compareToIgnoreCase("on") == 0 )
        {

            if(mf != null)
            {
                plugin.fMessage(player, plugin.msgs.getMessage("CMD_NOTIFY_ON"));
                //fMessage(player,"Friends: Turning on join/quit notifications.");
                mf.notify = true;
                plugin.friendsInfo.AddPlayer(player.getName().toLowerCase().trim(),mf);
                UpdatePlayerSection(player.getName().toLowerCase().trim(), mf);
                return;
            }

        }
        if(cmd.compareToIgnoreCase("off") == 0 )
        {

            if(mf != null)
            {
                plugin.fMessage(player, plugin.msgs.getMessage("CMD_NOTIFY_OFF"));
                //fMessage(player,"Friends: Turning off join/quit notifications.");
                mf.notify = false;
                plugin.friendsInfo.AddPlayer(player.getName().toLowerCase().trim(),mf);
                UpdatePlayerSection(player.getName().toLowerCase().trim(), mf);
                return;
            }

        }

        plugin.fMessage(player, plugin.msgs.getMessage("CMD_NOTIFY_HELP"));
        //fMessage(player,"You don't have any friends on your list yet. ");

    }


    public void List(Player player)
    {

        MyFriends mf = plugin.friendsInfo.GetPlayer(player.getName().toLowerCase().trim());
        if(mf != null)
        {
            if(mf.deny)
            {
                plugin.fMessage(player, plugin.msgs.getMessage("CMD_LIST_DENY_ON"));
                //fMessage(player,"You are currently denying all friends requests. /friends allow to change it");
                return;
            }

            if(mf.list!=null)
            {
                if(mf.list.size() > 0)
                {
                    plugin.fMessage(player, plugin.msgs.getMessage("CMD_LIST_TITLE"));
                    //fMessage(player,"Your Friends List");
                    plugin.fMessage(player, "==================");
                    for(String p : mf.list)
                    {
                        Player pFriend = plugin.getServer().getPlayer(p.toLowerCase().trim());
                        if(pFriend!=null)
                        {
                            if(player.canSee(pFriend))
                                plugin.fMessage(player, plugin.msgs.getMessage("CMD_LIST_ONLINE", p));
                            else
                                plugin.fMessage(player, plugin.msgs.getMessage("CMD_LIST_OFFLINE", p));
                            //fMessage(player,ChatColor.WHITE+p+" is online.");
                        }
                        else
                        {
                            plugin.fMessage(player, plugin.msgs.getMessage("CMD_LIST_OFFLINE", p));
                            //fMessage(player,ChatColor.RED+p+" is offline");
                        }

                    }

                    return;
                }

            }
        }

        plugin.fMessage(player, plugin.msgs.getMessage("CMD_LIST_EMPTY"));
        //fMessage(player,"You do not have anyone on your friends list."    }
    }
    
    public void Pending(Player player)
    {
        MyFriends mf = plugin.friendsInfo.GetPlayer(player.getName().toLowerCase().trim());
        if(mf.pending!=null)
        {
            plugin.getLogger().info("Pending Exists...");
            if(mf.pending.size()>0)
            {
                player.sendMessage(plugin.msgs.getMessage("CMD_PENDING_TITLE"));
                player.sendMessage(plugin.msgs.getMessage("CMD_PENDING_ACCEPT_INSTRUCTIONS"));
                player.sendMessage(plugin.msgs.getMessage("CMD_PENDING_DECLINE_INSTRUCTIONS"));

                player.sendMessage(ChatColor.AQUA+"[Friends] "+"----------------------------------");

                for(String name:mf.pending)
                {
                    player.sendMessage(ChatColor.AQUA+"[Friends] "+name);
                }
                return;
            }

        }
        player.sendMessage(plugin.msgs.getMessage("CMD_PENDING_NONE"));
        return;
    }


    public void Deny(Player player)
    {


        MyFriends mf = plugin.friendsInfo.GetPlayer(player.getName().toLowerCase().trim());

        if (mf == null)
        {
            mf = new MyFriends();
            CreatePlayerSection(player.getName().toLowerCase().trim(),mf);
        }
        mf.SetDeny(true);

        //Need to Clean House here... grumble...
        for(String pf : mf.list)
        {
            MyFriends mf2 =  plugin.friendsInfo.GetPlayer(pf.toLowerCase().trim());
            mf2.RemoveListing(player.getName().toLowerCase().trim());

            plugin.fMessage(player, plugin.msgs.getMessage("CMD_DENY_PLAYER_REMOVED", pf));
            //fMessage(player,"Removed "+pf+" from Friends list. Sending notice to player.");
            if(plugin.getServer().getPlayer(pf.toLowerCase().trim())!=null)
            {
                plugin.fMessage(plugin.getServer().getPlayer(pf), plugin.msgs.getMessage("CMD_DENY_FRIEND_NOTICE", player.getName()));
                //fMessage(plugin.getServer().getPlayer(pf),player.getName()+ " is no longer participating in the friends list.");
            }
            else
            {
                mf2.AddNotice(plugin.msgs.getMessage("CMD_DENY_FRIEND_NOTICE",player.getName()));
                //mf2.AddNotice(player.getName()+ " is no longer participating in the friends list.");
            }


            plugin.friendsInfo.AddPlayer(pf.toLowerCase().trim(),mf2);
            UpdatePlayerSection(pf.toLowerCase().trim(), mf2);
        }

        mf.ClearListing();
        mf.ClearPending();

        plugin.friendsInfo.AddPlayer(player.getName().toLowerCase().trim(),mf);
        UpdatePlayerSection(player.getName().toLowerCase().trim(), mf);

        plugin.fMessage(player, plugin.msgs.getMessage("CMD_DENY_COMPLETE"));
        //fMessage(player,"Now denying all friends requests and notifications.");
        return;
    }



 
    
    public void Allow(Player player)
    {

        MyFriends mf = plugin.friendsInfo.GetPlayer(player.getName().toLowerCase().trim());
        if(mf!=null)
        {
            mf.SetDeny(false);
            plugin.friendsInfo.AddPlayer(player.getName().toLowerCase().trim(),mf);

            UpdatePlayerSection(player.getName().toLowerCase().trim(), mf);

            plugin.fMessage(player, plugin.msgs.getMessage("CMD_ALLOW_COMPLETE"));
            //fMessage(player,("Now allowing all friends requests and notifications again."));
        }

    }

    public void CreatePlayerSection(String player,MyFriends mf)
    {
        ConfigurationSection cs;
        ConfigurationSection ps = plugin.playerdata.getCustomConfig().getConfigurationSection("players");
        if (ps == null) {
            plugin.getLogger().info( "Creating Section for Players ");

            ps = plugin.playerdata.getCustomConfig().createSection("players");
        }


        plugin.getLogger().info( "Creating Section for Players :  " +player);
        cs = plugin.playerdata.getCustomConfig().createSection("players." +player.toLowerCase().trim());

        cs.set("Friends", mf.list);
        cs.set("Pending", mf.pending);
        cs.set("Deny",mf.deny);
        cs.set("Notify",mf.notify);
    }



    public void UpdatePlayerSection(String player,MyFriends mf)
    {
        ConfigurationSection cs;
        cs =  plugin.playerdata.getCustomConfig().getConfigurationSection("players." + player.toLowerCase().trim());
        if(cs == null)
        {

            ConfigurationSection ps = plugin.playerdata.getCustomConfig().getConfigurationSection("players");
            if (ps == null) {
                plugin.getLogger().info( "Creating Section for Players ");

                ps = plugin.playerdata.getCustomConfig().createSection("players");
            }

            plugin.getLogger().info( "Creating Section for Players :  " +player);
            cs = plugin.playerdata.getCustomConfig().createSection("players." +player.toLowerCase().trim());
        }

        cs.set("Friends", mf.list);
        cs.set("Pending", mf.pending);
        cs.set("Deny",mf.deny);
        cs.set("Notify",mf.notify);
        cs.set("Notices",mf.notices);
    }
}
