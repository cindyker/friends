package com.minecats.cindyk.friends;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;

/**
 * Created with IntelliJ IDEA.
 * User: cindy
 * Date: 10/3/13
 * Time: 8:36 PM
 *
 *
 */
public class PlayerData {


    static FileConfiguration customConfig = null;
    File customConfigFile = null;
    Friends plugin;

    PlayerData(Friends plugin)
    {
        this.plugin = plugin;
    }

    public void reloadCustomConfig() {

        //TODO: Consider that this is a hardcoded filename... ick.
        if (customConfigFile == null) {

            customConfigFile = new File(plugin.getDataFolder().getAbsolutePath(), "PlayerData.yml");
            if (!customConfigFile.exists()) {
                try
                {
                    new File(plugin.getDataFolder().getAbsolutePath()).mkdirs();
                 //   customConfigFile.mkdir();
                    customConfigFile.createNewFile();
                }
                catch(IOException ex)
                {
                    plugin.getLogger().log(Level.SEVERE, "Failed to create data file. " + customConfigFile, ex);
                }
            }
        }
        customConfig = YamlConfiguration.loadConfiguration(customConfigFile);

        /*  // Look for defaults in the jar
        InputStream defConfigStream = mama.getResource("PlayerData.yml");
        if (defConfigStream != null) {
        YamlConfiguration defConfig = YamlConfiguration.loadConfiguration(defConfigStream);
        customConfig.setDefaults(defConfig);
        }*/
    }

    public FileConfiguration getCustomConfig() {
        if (customConfig == null) {
            this.reloadCustomConfig();
        }
        return customConfig;
    }


    public void saveCustomConfig() {
        if (customConfig == null || customConfigFile == null) {
            plugin.getLogger().info( "no customConfig here");
            return;
        }
        try {
            plugin.getLogger().info( "saveCustomConfig...");
            getCustomConfig().save(customConfigFile);
        } catch (IOException ex) {
            plugin.getLogger().log(Level.SEVERE, "Could not save config to " + customConfigFile, ex);
        }
    }

    public void SaveItToDisk() {
        //saveCustomConfig();
        try {
            customConfig.save(customConfigFile);
        } catch (IOException e) {
            plugin.getLogger().log(Level.SEVERE, "Could not save config to " + customConfigFile, e);
            //	  logger.severe(PREFIX + " error writting configurations");
            e.printStackTrace();
        }
    }

}
