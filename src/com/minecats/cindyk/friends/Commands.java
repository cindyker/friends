package com.minecats.cindyk.friends;

/**
 * Created with IntelliJ IDEA.
 * User: cindy
 * Date: 10/3/13
 * Time: 8:39 PM
 * To change this template use File | Settings | File Templates.
 */

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.plugin.PluginDescriptionFile;

import static org.bukkit.Bukkit.getServer;


public class Commands implements CommandExecutor, Listener{

    private Friends plugin;
    @SuppressWarnings("unused")
    private String name;




    public Commands(Friends plugin)
    {
        this.plugin = plugin;
        name = plugin.getName();
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
    {

        //if not a player... we are done.
        if (!(sender instanceof Player)) {
            return false;
        }
        //OK.. we are a player.. lets play
        Player player = (Player) sender;



        String comm = cmd.getName().toLowerCase();

        //player.sendMessage(comm + " we sent ");
        if (args == null || args.length == 0) {
            plugin.fMessage(player, "/friends help ");
            return true;
        }

        //This SHOULD NEVER HAPPEN!
        if(comm.compareToIgnoreCase("friends")!=0)
        {
            return false;
        }

        switch (args[0]) {
            case "version":
            {
                PluginDescriptionFile pdf = plugin.getDescription(); //Gets plugin.yml
                //Gets the version
                player.sendMessage("Friends Version: " + pdf.getVersion());
               return true;
            }

            case "help":
            {

                if(args.length != 1)
                {
                    plugin.fMessage(player, plugin.msgs.getMessage("CMD_ERROR_HELP"));
                    //fMessage(player,"/friends help ");

                }
                player.sendMessage(ChatColor.AQUA+plugin.msgs.getMessage("HELP_TITLE"));
                player.sendMessage(plugin.msgs.getMessage("CMD_ADD_HELP"));
                player.sendMessage(plugin.msgs.getMessage("CMD_ACCEPT_HELP"));
                player.sendMessage(plugin.msgs.getMessage("CMD_DECLINE_HELP"));
                player.sendMessage(plugin.msgs.getMessage("CMD_REMOVE_HELP"));
                player.sendMessage(plugin.msgs.getMessage("CMD_NOTIFY_HELP"));
                player.sendMessage(plugin.msgs.getMessage("CMD_LIST_HELP"));
                player.sendMessage(plugin.msgs.getMessage("CMD_DENY_HELP"));
                player.sendMessage(plugin.msgs.getMessage("CMD_ALLOW_HELP"));
                player.sendMessage(plugin.msgs.getMessage("CMD_PENDING_HELP"));
                player.sendMessage(ChatColor.AQUA+"================================");

                return true;
            }

            case "add":
            {
                plugin.getLogger().info(player.getName()+" called add. ");

                if(args.length < 2)
                {
                    plugin.fMessage(player, plugin.msgs.getMessage("CMD_ADD_HELP"));
                    return  true;
                }

                plugin.friendFunctions.AddFriend(player,args[1]);

                return true;
            }


            case "accept":
            {
                if(args.length != 2)
                {
                    plugin.fMessage(player, plugin.msgs.getMessage("CMD_ACCEPT_HELP"));
                    //fMessage(player,"/friends accept [playername] - To make a friends request ");
                    return true;
                }

                plugin.friendFunctions.AcceptFriend(player,args[1]); 
                return true;

            }

            case "decline":
            {
                if(args.length != 2)
                {
                    plugin.fMessage(player, plugin.msgs.getMessage("CMD_DECLINE_HELP"));
                    //fMessage(player,"/friends decline [playername] - To decline a friends request ");
                    return true;
                }

                String FriendName = args[1].toLowerCase();
                plugin.friendFunctions.DeclineFriend(player,FriendName);
                return true;
            }


            case "remove":
            {
                if(args.length != 2)
                {
                    plugin.fMessage(player, plugin.msgs.getMessage("CMD_REMOVE_HELP"));
                    //fMessage(player,"/friends remove [playername] - To remove a player from the list ");
                    return true;
                }
                  
                plugin.friendFunctions.RemoveFriend(player,args[1].toLowerCase().trim());

                return true;

            }


            case "notify":
            {
                if(args.length != 2)
                {
                    plugin.fMessage(player, plugin.msgs.getMessage("CMD_NOTIFY_HELP"));
                    //fMessage(player,"/friends notify [on/off] - to turn on and off join/quit notifications. ");
                    return true;
                }

                plugin.friendFunctions.Notify(player,args[1]);

                return true;

            }


            case "list":
            {
                if(args.length != 1)
                {
                   plugin.fMessage(player, plugin.msgs.getMessage("CMD_LIST_HELP"));
                    //fMessage(player,"/friends list - Displays your current friends list.  ");
                    return true;
                }

                plugin.friendFunctions.List(player);
                return true;
            }

            case "pending":
            {
                if(args.length != 1)
                {
                    plugin.fMessage(player, plugin.msgs.getMessage("CMD_PENDING_HELP"));
                    return true;
                }
                plugin.friendFunctions.Pending(player);
                return true;
            }

            case "deny":
            {

                if(args.length != 1)
                {
                    plugin.fMessage(player, plugin.msgs.getMessage("CMD_DENY_HELP"));
                    //fMessage(player,"/friends deny - Decline all friends requests  ");
                    return true;
                }
                plugin.friendFunctions.Deny(player);
                return true;

            }

            case "allow":
            {
                if(args.length != 1)
                {
                    plugin.fMessage(player, plugin.msgs.getMessage("CMD_ALLOW_HELP"));
                    //fMessage(player,"/friends allow - Allow friends requests again ");
                    return true;
                }
                plugin.friendFunctions.Allow(player);

               return true;
            }

        }

        plugin.fMessage(player,plugin.msgs.getMessage("CMD_ERROR_HELP"));
        //player.sendMessage("/friends help ");
        return true;

    }

 




}
