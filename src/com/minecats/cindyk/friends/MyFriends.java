package com.minecats.cindyk.friends;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: cindy
 * Date: 10/3/13
 * Time: 8:46 PM
 *
 */
public class MyFriends {

    List<String> list = new ArrayList<String>();
    List<String> pending = new ArrayList<String>();
    List<String> notices = new ArrayList<String>();

    boolean deny;
    boolean notify;

    public MyFriends()
    {
        deny = false;
        notify = true;
    }

    public List<String> getNotices()
    {
        return notices;
    }

    public void AddNotice(String strNotice)
    {

        notices.add(strNotice);
    }

    public void RemoveNotice(String strNotice)
    {
        notices.remove(strNotice);
    }

    public void DisplayNotices(Player pl)
    {
        for(String note: notices)
        {
            pl.sendMessage(ChatColor.AQUA+"[Friends] "+note);
            notices.remove(note);
        }
    }

    public void SetDeny(boolean d)
    {
        deny = d;
    }

    public boolean GetDeny()
    {
        return deny;
    }

    public void SetNotify(boolean d)
    {
        notify = d;
    }

    public boolean GetNotify()
    {
        return notify;
    }

    public boolean IsPending(String player)
    {

        for(String p : pending)
        {
            if(player.compareToIgnoreCase(p)==0)
            {
                //Player is Already on the List
                return true;
            }
        }

        return false;
    }

    public boolean OnList(String player)
    {

        for(String p : list)
        {
            if(player.compareToIgnoreCase(p)==0)
            {
                //Player is Already on the List
                return true;
            }
        }

        return false;
    }

    public boolean AddPending(String player)
    {
        for(String p : pending)
        {
            if(player.compareToIgnoreCase(p)==0)
            {
                //Player is Already on the List
                return false;
            }
        }

        for(String p : list)
        {
            if(player.compareToIgnoreCase(p)==0)
            {
                //Player is Already on the List
                return false;
            }
        }

        pending.add(player.toLowerCase().trim());
        return true;

    }

    public boolean AddToList(String player)
    {
//        for(String p : pending)
//        {
//            if(player.compareToIgnoreCase(p)==0)
//            {
//                //Player is Already on the List
                pending.remove(player.toLowerCase().trim());
//            }
//        }

        for(String p : list)
        {
            if(player.compareToIgnoreCase(p)==0)
            {
                //Player is Already on the List
                return false;
            }
        }

        list.add(player.toLowerCase().trim());
        return true;

    }

    public boolean RemovePending(String player)
    {
         //Player is Already on the List
         pending.remove(player.toLowerCase().trim());
         return true;

    }


    public boolean RemoveListing(String player)
    {
        for(String p : list)
        {
            if(player.compareToIgnoreCase(p)==0)
            {
                //Player is Already on the List
                list.remove(p.toLowerCase().trim());
                return true;
            }
        }
        return false;
    }

    public void ClearListing()
    {
        list.clear();
    }

    public void ClearPending()
    {
       pending.clear();
    }

}
