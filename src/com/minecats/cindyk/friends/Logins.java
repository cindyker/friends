package com.minecats.cindyk.friends;

import org.bukkit.ChatColor;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerQuitEvent;

/**
 * Created with IntelliJ IDEA.
 * User: cindy
 * Date: 10/3/13
 * Time: 8:39 PM
 *
 */
public class Logins  implements Listener {

    Friends plugin;

    public Logins(Friends _plugin) {
        plugin = _plugin;

    }


    @EventHandler(priority = EventPriority.LOW) // Makes your event Low priority
    void onPlayerKick(PlayerKickEvent plog) {

            PlayerLeaving(plog.getPlayer());

    }

    @EventHandler(priority = EventPriority.LOW) // Makes your event Low priority
    void onPlayerQuit(PlayerQuitEvent plog) {

            PlayerLeaving(plog.getPlayer());

    }

    ///////////////////////////////////////
    //
    //
    //////////////////////////////////////
    void PlayerLeaving(Player pp) {

        FileConfiguration customConfig = null;

        plugin.getLogger().info( "Player Logout: " + pp.getName().toLowerCase().trim());

        MyFriends mf = plugin.friendsInfo.GetPlayer(pp.getName().toLowerCase().trim());
        //If they do not have a friends object, no point in going thru file work.
        if(mf==null)
            return;

        for(String fname: mf.list)
        {
            Player friend = plugin.getServer().getPlayer(fname.toLowerCase().trim());

            if(friend!=null)
            {
                MyFriends pmf =   plugin.friendsInfo.GetPlayer(fname.toLowerCase().trim());
                if(pmf.notify)
                    friend.sendMessage(ChatColor.AQUA + "[Friends] " + ChatColor.RED + pp.getName() + " has logged off.");
            }

        }


        customConfig = plugin.playerdata.getCustomConfig();
        Player pl = pp;

        ConfigurationSection cs = customConfig.getConfigurationSection("players." + pl.getName().toLowerCase());

        //look up player in config
        //
        //If the player doesn't have a section already, then we want to do this...
        if (cs == null) {


            ConfigurationSection ps = customConfig.getConfigurationSection("players");
            if (ps == null) {
                plugin.getLogger().info( "Creating Section for Players ");
                cs = customConfig.createSection("players");
            }
            plugin.getLogger().info( "Creating Section for Players :  " + pl.getName().toLowerCase().trim());
            cs = customConfig.createSection("players." + pl.getName().toLowerCase().toLowerCase().trim());
        }

        cs.set("Friends", mf.list);
        cs.set("Pending", mf.pending);
        cs.set("Deny",mf.deny);
        cs.set("Notify",mf.notify);
        cs.set("Notices",mf.notices);

        //Do we want this Disk IO on every logout..or do we
        //just want to wait for server stop.
        plugin.getLogger().info( "Saving to Disk...");
       // plugin.playerdata.SaveItToDisk();
        plugin.playerdata.saveCustomConfig();
        plugin.playerdata.reloadCustomConfig();


    }

    ///////////////////////////////////////////
    //Function:
    //   onPlayerLogin
    //
    //  Responds to Player Login event.
    ///////////////////////////////////////////
    @EventHandler(priority = EventPriority.LOW) // Makes your event Low priority
    void onPlayerLogin(PlayerLoginEvent plog) {
        String curChannel;
        FileConfiguration customConfig = null;
        // String pFormatted = "";
        Player pl = plog.getPlayer();

       plugin.getLogger().info("[Friends] Player Login Event; " + pl.getName());



    }

    @EventHandler
    public void onLogin(PlayerJoinEvent event) {

        Player pl = event.getPlayer();

        plugin.getLogger().info("[Friends] Player Login Event; " + pl.getName());

        MyFriends mf = plugin.friendsInfo.GetPlayer(pl.getName().toLowerCase().trim());

        //player hasn't been on before on, not since last restart.
        if(mf == null)
            return;

        ///////////////////////////////////
        // Let the Friends know
        for(String fname: mf.list)
        {
            Player friend = plugin.getServer().getPlayer(fname.toLowerCase().trim());

            if(friend!=null)
            {

                if(friend.canSee(pl))
                {
                    MyFriends pmf =   plugin.friendsInfo.GetPlayer(fname.toLowerCase().trim());
                    if(pmf.notify)
                        friend.sendMessage(ChatColor.AQUA+"[Friends] "+ChatColor.WHITE+pl.getName()+ " has logged in.");
                }
            }

        }


        if(mf.notices!=null)
        {
            if(mf.notices.size()>0)
            {
                pl.sendMessage(ChatColor.AQUA+"[Friends] "+ "While you were away...");
                mf.DisplayNotices(pl);
            }
        }

        if(mf.pending!=null)
        {
            plugin.getLogger().info("Pending Exists...");
            if(mf.pending.size()>0)
            {
                plugin.getLogger().info(ChatColor.AQUA+"[Friends] "+"Pending Friends");

                pl.sendMessage(plugin.msgs.getMessage("CMD_PENDING_TITLE"));
              //  pl.sendMessage(ChatColor.AQUA+"[Friends] "+"You have pending Friends requests.");
                pl.sendMessage(plugin.msgs.getMessage("CMD_PENDING_ACCEPT_INSTRUCTIONS"));
                pl.sendMessage(plugin.msgs.getMessage("CMD_PENDING_DECLINE_INSTRUCTIONS"));
                //pl.sendMessage(ChatColor.AQUA+"[Friends] "+"/friends accept [name] to accept");
                //pl.sendMessage(ChatColor.AQUA+"[Friends] "+"/friends decline [name] to decline");
                pl.sendMessage(ChatColor.AQUA+"[Friends] "+"----------------------------------");

                for(String name:mf.pending)
                {
                    pl.sendMessage(ChatColor.AQUA+"[Friends] "+name);
                }

            }
        }

        if(mf.notify)
        {
            if(mf.list!=null)
            {
                if(mf.list.size()>0)
                {
                   pl.sendMessage(ChatColor.AQUA+"[Friends] "+ChatColor.WHITE+ChatColor.WHITE+ " ---Status---");
                    for(String fr : mf.list)
                    {
                        Player pf = plugin.getServer().getPlayer(fr.toLowerCase().trim());


                        if(pf!=null)
                            pl.sendMessage(ChatColor.AQUA+"[Friends] "+ChatColor.WHITE+fr+" is online.");
                        else
                            pl.sendMessage(ChatColor.AQUA+"[Friends] "+ChatColor.RED+fr+" is offline");
                    }

                    pl.sendMessage(ChatColor.AQUA+"[Friends] "+ChatColor.WHITE+" -----------");
                }
            }
        }
    }

}
