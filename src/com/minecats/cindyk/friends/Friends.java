package com.minecats.cindyk.friends;

import org.bukkit.ChatColor;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import java.sql.*;
import com.mysql.jdbc.jdbc2.optional.MysqlConnectionPoolDataSource;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.milkbowl.vault.permission.Permission;

/**
 * Created with IntelliJ IDEA.
 * User: cindy
 * Date: 10/3/13
 * Time: 7:28 PM
 *
 * Friends Plugin for Bukkit Minecraft Servers
 *
 * Appearance:
 /friend add Notch:
 Notch has requested to be your friend! To accept, do /friend accept Notch

 /friend accept Notch:
 Notch has became your friend!

 /friend ignore Notch:
 Friend request from Notch, has been ignored!

 /friend remove Notch:
 Notch has been removed from your friends list!

 /friend notifications on: (On Player Join/Leave)
 Player has left the server!

 Notch has joined the server!

 /friends list:
 Player is online.
 Notch is online.
 Jeb is offline.
 Blocky is offline
 Example is offline.

 */



public class Friends extends JavaPlugin implements Listener {
    Logger log;
    private Permission permission;
    public PlayerData playerdata;
    public FriendsInfo friendsInfo;
    public FriendFunctions friendFunctions;

    public Logins logins;
    public Commands cmds;
    public Messages msgs;

    public  String Version;

    public final String playertbl = "Players";
    public final String friendtbl = "Friends";

    private com.minecats.cindyk.friends.MiniConnectionPoolManager pool;


    @Override
    public void onEnable() {
        super.onEnable();

        log = getLogger();

        loadConfig();

        if(this.getConfig().getBoolean("Friends.MySQL.Enabled"))
        {
            try{
                connect();
                setUpSQL();

            }catch(ClassNotFoundException cex)
            {
                this.getLogger().log(Level.SEVERE," Error:" + cex.getMessage());
            }
            catch(SQLException ex)
            {
                this.getLogger().log(Level.SEVERE," Error:" + ex.getMessage());
            }


        }
        else
        {

        }

        getServer().getPluginManager().registerEvents(this, this);


        log.info(String.format("[%s] - Initializing...", getDescription().getName()));

        log.info(String.format("[%s] - Checking for Vault...", getDescription().getName()));

        // Set up Vault
        if(!setupPermissions()) {
            log.info(String.format("[%s] - Could not find Vault dependency, disabling.", getDescription().getName()));
        }

        logins = new Logins(this);
        getServer().getPluginManager().registerEvents(logins, this);

        cmds = new Commands(this);
        msgs = new Messages(this);
        friendFunctions = new FriendFunctions(this);
        msgs.LoadStrings();

        log.info(String.format("[%s] - Loading player data", getDescription().getName()));
        playerdata = new PlayerData(this);
        playerdata.reloadCustomConfig();

        friendsInfo = new FriendsInfo(this);
        LoadFriends();

        getCommand("friends").setExecutor(cmds);

    }

    @Override
    public void onDisable() {
        super.onDisable();

        playerdata.SaveItToDisk();
    }


    private void LoadFriends()
    {

        ConfigurationSection ps = playerdata.getCustomConfig().getConfigurationSection("players");

        if(ps!=null)
        {
            for(String name:ps.getKeys(false))
            {
                MyFriends mf = new MyFriends();

                //this.getLogger().info("Loading data from file: " + name);

                ConfigurationSection cs = playerdata.getCustomConfig().getConfigurationSection("players."+name) ;
                if(cs != null)
                {
                    mf.list = cs.getStringList("Friends");
                    mf.pending = cs.getStringList("Pending");
                    mf.SetDeny(cs.getBoolean("Deny",false));
                    mf.SetNotify(cs.getBoolean("Notify",true));
                   friendsInfo.AddPlayer(name,mf);
                }
            }

            this.getLogger().info("[Friends] Players loaded; " + friendsInfo.myMap.size());
        }
        else
        {
            this.getLogger().info("[Friends] No Players to load; ");
        }



    }

    private boolean setupPermissions()
    {
        RegisteredServiceProvider<Permission> permissionProvider = getServer().getServicesManager().getRegistration(net.milkbowl.vault.permission.Permission.class);
        if (permissionProvider != null) {
            permission = permissionProvider.getProvider();
        }
        return (permission != null);
    }


    private void setUpSQL()
    {

        this.getLogger().info("Connecting to SQL database!");
        {
            Connection connection = null;
            Statement st = null;
            int rs = 0;
            try
            {
                connection = pool.getValidConnection();
                    /*connection =
							DriverManager.getConnection("jdbc:MySQL://" + plugin.getConfig().getString("VoteSQL.MySQL.Server") + "/" + plugin.getConfig().getString("VoteSQL.MySQL.Database"), plugin.getConfig().getString("VoteSQL.MySQL.User"), plugin.getConfig().getString("VoteSQL.MySQL.Password"));*/					st = connection.createStatement();
                rs = st.executeUpdate("CREATE TABLE IF NOT EXISTS "
                        + this.getConfig().getString("Friends.MySQL.Table_Prefix") + playertbl
                        + "( uuid VARCHAR(36) NOT NULL, playername VARCHAR(32), dateadded DATETIME, lastseen DATETIME, PRIMARY KEY (uuid,playername))");

                rs = st.executeUpdate("CREATE TABLE IF NOT EXISTS "
                        + this.getConfig().getString("Friends.MySQL.Table_Prefix") + friendtbl
                        + "( uuid VARCHAR(36) NOT NULL, frienduuid VARCHAR(36) NOT NULL, status VARCHAR(10), dateadded DATETIME, PRIMARY KEY (uuid,frienduuid))");
                this.getLogger().info("SQL database connected!");
            }
            catch (SQLException e)
            {
                e.printStackTrace();
                this.getLogger().log(Level.SEVERE," Error:" + rs);
            }
            catch(MiniConnectionPoolManager.TimeoutException te)
            {
                this.getLogger().log(Level.SEVERE,"Timeout Exception Error:" + te.getMessage());
            }
        }
        return;
    }

    public synchronized void connect() throws ClassNotFoundException, SQLException {

        Class.forName("com.mysql.jdbc.Driver");
        getLogger().info("MySQL driver loaded");
        MysqlConnectionPoolDataSource dataSource = new MysqlConnectionPoolDataSource();
        dataSource.setDatabaseName(this.getConfig().getString("Friends.MySQL.Database"));
        dataSource.setServerName(this.getConfig().getString("Friends.MySQL.Server"));
        dataSource.setPort(3306);
        dataSource.setUser(this.getConfig().getString("Friends.MySQL.User"));
        dataSource.setPassword(this.getConfig().getString("Friends.MySQL.Password"));
        pool = new MiniConnectionPoolManager(dataSource, 1);
        getLogger().info("Connection pool ready");

    }



    private void loadConfig() {

        String path2 = "Friends.MySQL.Enabled";
        String path3 = "Friends.MySQL.Server";
        String path4 = "Friends.MySQL.Database";
        String path5 = "Friends.MySQL.User";
        String path6 = "Friends.MySQL.Password";
        String path7 = "Friends.MySQL.Table_Prefix";

        getConfig().addDefault(path2, false);
        getConfig().addDefault(path3, "Server Address eg.Localhost");
        getConfig().addDefault(path4, "Place Database name here");
        getConfig().addDefault(path5, "Place User of MySQL Database here");
        getConfig().addDefault(path6, "Place User password here");
        getConfig().addDefault(path7, "Friends");

        getConfig().options().copyDefaults(true);
        saveConfig();
    }


    public void addData(String name, String strUUID ) throws ClassNotFoundException
    {
        PreparedStatement pst = null;
        Connection con = null;
        Statement uuidstmt = null;
        ResultSet rs = null;
        // int num = 1;

        if(strUUID == null)
        {
            getLogger().info("Player UUID is MiSSING!! " + name);
            return;
        }


        try
        {
            Class.forName("com.mysql.jdbc.Driver");
            getLogger().info("MySQL driver loaded");
            MysqlConnectionPoolDataSource dataSource = new MysqlConnectionPoolDataSource();
            dataSource.setDatabaseName(getConfig().getString("Friends.MySQL.Database"));
            dataSource.setServerName(getConfig().getString("Friends.MySQL.Server"));
            dataSource.setPort(3306);
            dataSource.setUser(getConfig().getString("Friends.MySQL.User"));
            dataSource.setPassword(getConfig().getString("Friends.MySQL.Password"));

            pool = new MiniConnectionPoolManager(dataSource, 10);

            con = pool.getValidConnection();

            String table = getConfig().getString("Friends.MySQL.Table_Prefix") +playertbl;

            getLogger().info("Looking for UUID :  " + strUUID );
            uuidstmt = con.createStatement();
            if(uuidstmt.execute("SELECT * FROM " + table + " wHERe uuid = '" +strUUID+"'"))
            {
                rs = uuidstmt.getResultSet();
                boolean bFound = false;

                java.util.Date today = new java.util.Date();
                java.sql.Timestamp timestamp = new java.sql.Timestamp(today.getTime());


                while (rs.next())
                // if (!rs.next())
                {


                    if(rs.getString("playername").compareToIgnoreCase(name)==0)
                    {
                        bFound = true;
                        //HAS A NEW NAME!
                        //TELL PLAYER THEY ARE SCREWED!
                        getLogger().info("Player: " + name + " has been here before.");

                        //Update LastSeen!


                        //INSERT INTO DATABASE.
                        pst = con.prepareStatement("Update " + table+ " Set lastseen = ? where (uuid = ? and playername = ? ) ");

                        pst.setTimestamp(1, timestamp);
                        pst.setString(2, strUUID);
                        pst.setString(3, name);

                        pst.executeUpdate();
                        getLogger().info("Updated Player");


                    }

                }

                if(!bFound)
                {

                    //INSERT INTO DATABASE.
                    pst = con.prepareStatement("INSERT INTO " + table
                            + "(uuid,playername,dateadded,lastseen) VALUES(?, ? , ? , ?)");


                    pst.setString(1, strUUID);
                    pst.setString(2, name);
                    pst.setTimestamp(3, timestamp);
                    pst.setTimestamp(4, timestamp);

                    pst.executeUpdate();
                    getLogger().info("Inserted Player");
                    //System.out.print("inserted");
                }
            }


        }
        catch (SQLException ex)
        {
            System.out.print(ex);
        } finally {
            close(rs);
            close(uuidstmt);
            close(con);
        }
    }
    public synchronized void close() {
        try {
            pool.dispose();
        } catch (SQLException ex) {
            getLogger().info(ex.getMessage());
        }
    }

    public void reload() {
    }


    public  void close(Statement st) {
        if (st != null) {
            try {
                st.close();
            } catch (SQLException ex) {
                getLogger().info(ex.getMessage());
            }
        }
    }

    public  void close(ResultSet rs) {
        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException ex) {
                getLogger().info(ex.getMessage());
            }
        }
    }

    public  void close(Connection con) {
        if (con != null) {
            try {
                con.close();
            } catch (SQLException ex) {
                getLogger().info(ex.getMessage());
            }
        }
    }

    public void fMessage(Player p, String msg)
    {
        p.sendMessage(ChatColor.AQUA+"[Friends] "+ChatColor.WHITE+msg);
    }





}
