package com.minecats.cindyk.friends;

import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.*;
import java.util.HashMap;
import java.util.logging.Level;

/**
 * Created with IntelliJ IDEA.
 * User: cindy
 * Date: 10/11/13
 * Time: 5:26 AM
 * Contains the code to read in the inGame messages from the messages.yml
 */
public class Messages {

    static FileConfiguration customConfig = null;
    File customConfigFile = null;
    Friends plugin;

    HashMap<String, String> messages;


    Messages(Friends plugin)
    {
        this.plugin = plugin;
        messages = new HashMap<String,String>();

    }


    public void LoadStrings()
    {
        customConfig = getCustomConfig();

        for(String k:customConfig.getKeys(false))
        {
            plugin.getLogger().info("Adding :"+k+" to the list " +customConfig.getString(k));
            messages.put(k,customConfig.getString(k));
        }

    }


    public String getMessage(String key)
    {
        if(!messages.containsKey(key))
            return key;

        return ChatColor.translateAlternateColorCodes('&', messages.get(key));
    }

    public String getMessage(String key, Object...args)
    {
        if(!messages.containsKey(key))
            return key;

        return ChatColor.translateAlternateColorCodes('&', String.format(messages.get(key),args));
    }

    public void reloadCustomConfig() {

        //TODO: Consider that this is a hardcoded filename... ick.

        if (customConfigFile == null) {

            customConfigFile = new File(plugin.getDataFolder().getAbsolutePath(), "Messages.yml");
            if (!customConfigFile.exists()) {
                try
                {
                    new File(plugin.getDataFolder().getAbsolutePath()).mkdirs();
                    //   customConfigFile.mkdir();
                   // customConfigFile.createNewFile();

                   // InputStream defaults = plugin.getResource("Messages.yml");
                    File file = new File(plugin.getDataFolder().getAbsolutePath(), "Messages.yml");

                    InputStream de = plugin.getResource("Messages.yml");
                  //  File file2 = new File(de);
                  //  OutputStream output = new FileOutputStream(de);

                    YamlConfiguration def = YamlConfiguration.loadConfiguration(de);
                    def.save(file);
                    //customConfig.addDefaults(def);
                //    customConfig.options().copyDefaults(true);
                  //  customConfigFile.set("version", def.get("version"));
                  // getCustomConfig().save(customConfigFile);
                //  saveCustomConfig();
                }
                catch(IOException ex)
                {
                    plugin.getLogger().log(Level.SEVERE, "Failed to create data file. " + customConfigFile, ex);
                }

            }
        }
        customConfig = YamlConfiguration.loadConfiguration(customConfigFile);

        /*  // Look for defaults in the jar
        InputStream defConfigStream = mama.getResource("PlayerData.yml");
        if (defConfigStream != null) {
        YamlConfiguration defConfig = YamlConfiguration.loadConfiguration(defConfigStream);
        customConfig.setDefaults(defConfig);
        }*/
    }

    public FileConfiguration getCustomConfig() {
        if (customConfig == null) {
            this.reloadCustomConfig();
        }
        return customConfig;
    }


    public void saveCustomConfig() {
        if (customConfig == null || customConfigFile == null) {
            plugin.getLogger().info( "no customConfig here");
            return;
        }
        try {
            plugin.getLogger().info( "saveCustomConfig...");
            getCustomConfig().save(customConfigFile);
        } catch (IOException ex) {
            plugin.getLogger().log(Level.SEVERE, "Could not save config to " + customConfigFile, ex);
        }
    }

    public void SaveItToDisk() {
        //saveCustomConfig();
        try {
            customConfig.save(customConfigFile);
        } catch (IOException e) {
            plugin.getLogger().log(Level.SEVERE, "Could not save config to " + customConfigFile, e);
            //	  logger.severe(PREFIX + " error writting configurations");
            e.printStackTrace();
        }
    }


}
