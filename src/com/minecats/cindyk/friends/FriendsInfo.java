package com.minecats.cindyk.friends;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created with IntelliJ IDEA.
 * User: cindy
 * Date: 10/3/13
 * Time: 8:44 PM
 *
 */
public class FriendsInfo {

    Map<String,MyFriends> myMap = new ConcurrentHashMap<String,MyFriends>();
    Friends plugin;

    public FriendsInfo(Friends plugin)
    {
       this.plugin = plugin;
    }

    public MyFriends GetPlayerList(String player)
    {
        if(myMap.containsKey(player))
        {
            return myMap.get(player.toLowerCase().trim());
        }

        return null;

    }

    public void AddPlayer(String player,MyFriends mf)
    {
  //      plugin.getLogger().info("AddPLayer : " + player.toLowerCase());
        myMap.put(player.toLowerCase().trim(),mf);

    }

    public MyFriends GetPlayer(String player)
    {
//        plugin.getLogger().info("GetPLayer : " + player.toLowerCase());

       return myMap.get(player.toLowerCase().trim());
    }






}
